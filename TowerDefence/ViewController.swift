//
//  ViewController.swift
//  TowerDefence
//
//  Created by Nicolas Spragg and Nithan  on 2016-07-04.
//  Copyright © 2016 GG Games. All rights reserved.
//


import UIKit
import SceneKit
import SpriteKit


public enum gameState  {
    case inGame
    case inMenu
}

class ViewController: UIViewController {
    var scnView:SCNView!
    var menuScene:SCNScene!
    var gameScene:SCNScene!
    var towerScene:SCNScene!
    var playerScene:SCNScene!
    var towerNode:SCNNode!
    var startNode:SCNNode!
    var floorNode: SCNNode!
    var playerNode:SCNNode!
    
    var playerMenuNode:SCNNode!
    var towerMenuNode: SCNNode!
    var state = gameState.inMenu // game starts in the menu
    var spawnLocation : SCNVector3!
    
    var selectionState = 0 // 0 = nothing picked. 1 = picked
    
  
   

    override func viewDidLoad() {
        super.viewDidLoad()
       setupScene()
        
}
    
    /* 
     Loads the scene view in
     gets the location of the two scenes ( gameScene and menuScene) 
     sets the current scene to the meun 
     add in start node
     add delegate for rendering
 */
    func setupScene() {
        scnView = SCNView(frame: self.view.frame)
        self.view.addSubview(scnView)
        gameScene = SCNScene(named: "/TowerDefence.scnassets/gameScene.scn")
        menuScene = SCNScene(named: "/TowerDefence.scnassets/menuScene.scn")
        scnView.scene = menuScene
        
        
        startNode = menuScene.rootNode.childNodeWithName("start", recursively: true)
        floorNode = gameScene.rootNode.childNodeWithName("floor", recursively: true)
        scnView.delegate = self
        
}
    
    /*
     get the location of the tower scene
     create a tower node 
     add the tower to the gameScene
     move tower to the tapped location
 */
    func spawnTower(){
        
        towerScene = SCNScene(named: "/TowerDefence.scnassets/tower.scn")
       towerNode = towerScene.rootNode.childNodeWithName("tower", recursively: true )
        gameScene.rootNode.addChildNode(towerNode)
        towerNode.position = spawnLocation
        
        
}
    
    func spawnPlayer() {
        playerScene = SCNScene(named: "/TowerDefence.scnassets/player.scn")
        playerNode = playerScene.rootNode.childNodeWithName("player", recursively: true )
        gameScene.rootNode.addChildNode(playerNode)
        playerNode.position = spawnLocation
        
    }
    
    /*
     pause the scene
     create 1 second transition 
     run the transition 
     un pause the game scene 
     change the state to being in game.
 */
    func startGame() {
        
        menuScene.paused = true
        let transition = SKTransition.doorsOpenVerticalWithDuration(1.0)
        scnView.presentScene(gameScene, withTransition: transition, incomingPointOfView: nil, completionHandler: {
            self.gameScene.paused = false
            self.state = gameState.inGame
        })
}
    
    /* 
     
     check if node passed in has ID " start" 
     start the game if it does 
     else do nothing.
 
 */
    func handleTouch(node: SCNNode){
        if node.name == "start" {
            startGame()
        }
            itemToSpawnIn(node.name!)
    }
    
    
    
    
    func itemToSpawnIn (itemName: String) {
        
        if itemName == "tower" {
            selectionState = 1
        }
        
        if itemName == "player" {
            selectionState = 2
        }
        
        if itemName == "floor" && selectionState == 1 {
            spawnTower()
            
        }
        
        if itemName == "floor" && selectionState == 2 {
            spawnPlayer()
            
        }
        
    }
    
    /*
     Utility function to convert CGPoint to a SCNVector.
 
     */
    func CGPointToSCNVector3(view: SCNView, depth: Float, point: CGPoint) -> SCNVector3 {
        let projectedOrigin = view.projectPoint(SCNVector3Make(0, 0, depth))
        let locationWithz   = SCNVector3Make(Float(point.x), Float(point.y), projectedOrigin.z)
        return view.unprojectPoint(locationWithz)
    }
    
    
    //get when touch started
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch in touches {
            let location = touch.locationInView(scnView) // location tapped
             spawnLocation = CGPointToSCNVector3(scnView, depth: gameScene.rootNode.position.y, point: location) // convert the point
            
           

            let hitResults = scnView.hitTest(location, options: nil)
            
            if hitResults.count > 0 {
                // get the node tapped
                let result = hitResults.first!
                handleTouch(result.node)
                }
            }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    override func shouldAutorotate() -> Bool {
        return false
    }
    
   
        
}
    
    
    

// extension for the renderer. Updates at 60tick

extension ViewController: SCNSceneRendererDelegate {
    func renderer(renderer: SCNSceneRenderer, updateAtTime time: NSTimeInterval) {
        
        }

}